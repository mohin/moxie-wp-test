### Insturction 
Please follow the steps:  

- Download the plugin https://bitbucket.org/mohin/moxie-wp-test/downloads; 
- Unzip the file and put it into the WordPress plugins folder;
- Go to the Admin Panel => Plugins and activate the it;
- Add the appeared Movie posts;
- See the result on the home page.

### Features
- Scalable approuch;
- AngularJS rendering;
- Veraity of FX (you only need to change class names);
- Caching;
- No dependencies.