<?php

/**
 * Plugin Name: Moxie Movies
 * Plugin URI: https://getmoxied.net
 * Description: Adds new post type and displays a list of movies on the home page.
 * Version: 1.0
 * Author: Mohin Konstantin
 * Author URI: konstantin.mohin.me
 * License: GPL2
 */

namespace Moxie;


use Moxie\Entity\Movie;
use Moxie\Bootstrap;


final class Main
{

	private static $instance = null;


	public function __construct()
	{
		//loads necessary classes 
		$this->register_autoloader();

		//this will cause fatal error if someone try access to plugin directly
		add_action('init', array($this, 'init'));
	}

	public function init()
	{
		//loads hooks and cache
		Bootstrap::init();

		$movie = new Movie();

		//register new post type
		$movie->register();
	}

	public static function instance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}


	private function register_autoloader()
	{
		require_once(__DIR__ . '/src/Moxie/Autoloader.php');

		$autoloaders = array(
			array('\Moxie\Autoloader', 'autoload'),
		);


		foreach ($autoloaders as $autoloader) {
			spl_autoload_register($autoloader);
	   }
	}

}



Main::instance();

