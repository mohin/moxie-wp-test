
var movieListApp = angular.module('movieListApp', ['720kb.fx']);

var scripts = document.getElementsByTagName("script");
var currentScriptPath = scripts[scripts.length-1].src;



//Controller
movieListApp.controller('movieListCtrl', function($scope, $http, $timeout, $sce) {
	
	// convert string to html entity, if someone puts html characters into the Exerpt field in the Admin panel
	$scope.renderHtml = function (htmlCode) {
        return $sce.trustAsHtml(htmlCode);
    };

	$scope.init = function(){
		$scope.preload = 'Loading...';
		$scope.loaded = true;
		$scope.errors = 0;
	};

	// take a look at the line 27, without jQuery.param nothing work
	
	$http({
        url: ajaxurl,
        method: "POST",
        data: jQuery.param({"action": "movieList"}),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    })
    .then(function(response) {
    	console.log(response);
		if(response.data.error) {
			$scope.error = response.data.error;
			$scope.errors = 1;
		} else {
			
			$scope.movies = response.data;
			$scope.preload = 'Movies';
		}	
           
    });

           
});


//Directive

movieListApp.directive('moxieMovieList', function(){

	return {
		templateUrl: currentScriptPath.substring(0, currentScriptPath.lastIndexOf('/') - 2) +'template/movie_list.html',
		replace: true
	};
});