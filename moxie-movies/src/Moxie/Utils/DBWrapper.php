<?php 

namespace Moxie\Utils;


class DBWrapper {

	public static function update_meta($id, $key, $val)
	{
		update_post_meta($id, $key, $val);
	}


	public static function get_meta($id, $key, $single=true)
	{
		return get_post_meta( $id, $key, $single );
	}

	public static function get_entries($args)
	{
		return get_posts($args);
	}

	public static function get_thumb($id)
	{
		return wp_get_attachment_thumb_url($id);
	}
}