<?php 

namespace Moxie\Utils;


class FilterRegistrar
{
	public static function register($name, $callable, $priority = 10, $args = 10)
	{
		if (is_callable($callable)) {
			add_filter($name, $callable, $priority, $args);
		}
	}
}
