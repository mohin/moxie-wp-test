<?php 

namespace Moxie\Utils;

class HookRegistrar
{
	public static function register($name, $callable, $priority = 10, $accepted_args = 10)
	{
		if (is_callable($callable)) {
			add_action($name, $callable, $priority, $accepted_args);
		}
	}

	public static function unregister($name, $function_to_remove, $priority = 10)
	{
		remove_action($name, $function_to_remove, $priority);
	}
}