<?php 

namespace Moxie\Utils;

use Moxie\Utils\Helper;
use Moxie\Entity\Movie;
use Moxie\Bootstrap;


//if WP action goes here
if (!class_exists('\Cache'))
	Bootstrap::load_cache();


class Cache extends \Cache {
	
	const MOXIE_CACHE_NAME = 'MOXIE-MOVIE-LIST';

	const MOXIE_CACHE_EXT = '.cache';

	private $moxie_cachepath = '';

	// set default params for PHP-Simple-Cache
	function __construct()
	{
		$this->moxie_cachepath = sprintf( '%s/cache/', Helper::get_plugin_abs_path() );

		parent::__construct(
			array(
				'name' => static::MOXIE_CACHE_NAME,
				'path' => $this->moxie_cachepath,
				'extension' => static::MOXIE_CACHE_EXT
			)
		);
	}

	public function removeCache($entry_id)
	{
		// not clear cache when it's autosaving
		if (defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE) 
			return;

		if (defined('DOING_AJAX') && DOING_AJAX)
			return;
		//if user tap publish with an empty title
		//then goes back (cache already cleard), so we don't need to clear a ceche 
		$cache_cleared = $this->retrieve(Movie::CACHE_LIST_NAME);
		//remove cache only the certain type
		if(Helper::is_movie_post_type($entry_id) && !empty($cache_cleared)) {
			$this->erase(Movie::CACHE_LIST_NAME);
			$this->erase(Movie::CACHE_LIST_META);
		}
	}


}