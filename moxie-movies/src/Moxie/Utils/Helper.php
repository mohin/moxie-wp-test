<?php 

namespace Moxie\Utils;

use Moxie\Entity\Movie;


class Helper {

	public static function is_admin_section()
	{
		return is_admin();
	}


	public static function is_home_set()
	{
		return get_option( 'show_on_front' ) === 'page';
	}


	public static function is_home_page()
	{
		return is_front_page() || is_home();
	}


	public static function is_movie_post_type($id)
	{
		return get_post_type($id) === Movie::POST_TYPE;
	}


	public static function load_assets($name, $path, $type='css')
	{
		$asset_url = sprintf('%s/assets%s', self::get_plugin_url(), $path );

		if ($type === 'js')
			wp_enqueue_script( $name, $asset_url,  array('jquery'), '1.1', false );
		else
			wp_enqueue_style( $name, $asset_url );

	}


	public static function get_plugin_url()
	{
		return sprintf('%s/moxie-movies', WP_PLUGIN_URL);  
	}


	public static function get_plugin_abs_path()
	{
		//remove last slash from path
		return substr(plugin_dir_path(dirname(dirname(__DIR__))), 0, -1);
	}


}