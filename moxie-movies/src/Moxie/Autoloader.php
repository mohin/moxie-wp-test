<?php 

namespace Moxie;


class Autoloader
{
	
	public static function autoload($class)
	{
		//check namespace
		if (strpos($class, 'Moxie\\') !== 0 ) return;

		$classPath = str_replace('\\', '/', $class) . '.php';
		$classPath = dirname(dirname(__FILE__)) . '/' . $classPath;

		if (!is_file($classPath)) {
			throw new \Exception(sprintf('Class "%s" not found.', $classPath)); 
		}

		require_once($classPath);
	}
}