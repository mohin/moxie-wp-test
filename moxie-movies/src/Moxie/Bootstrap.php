<?php 

namespace Moxie;

use Moxie\EventListener\AdminListener;
use Moxie\EventListener\FrontListener;
use Moxie\Utils\Helper;

class Bootstrap
{
	public static function init()
	{
		self::load_hooks();
		self::load_cache();
	}

	private static function load_hooks()
	{
		foreach (self::get_hooks() as $hook) {
			$hook->register();
		}
	}

	private static function get_hooks()
	{
		$hooks = array();
		$hooks[] = new AdminListener();
		$hooks[] = new FrontListener();

		return $hooks;
	}

	// public if we need to load cache when WP action called
	public static function load_cache()
	{	
		$cache_path = sprintf( '%s/src/vendor/Simple-PHP-Cache/cache.class.php', Helper::get_plugin_abs_path());

		require_once($cache_path);


	}

}