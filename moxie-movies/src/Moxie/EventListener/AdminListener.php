<?php 

namespace Moxie\EventListener;

use Moxie\Entity\Movie;
use Moxie\Utils\HookRegistrar;
use Moxie\Utils\Helper;
use Moxie\Utils\Cache;


class AdminListener {

	public function register()
	{
		$movie = new Movie();
		$cache = new Cache();

		HookRegistrar::register('save_post', array($movie, 'saveMetabox'));
		HookRegistrar::register('save_post', array($cache, 'removeCache'));
		// user must enter movie title
		HookRegistrar::register('title_save_pre', array($movie, 'requireTitle'));
		//load assets for admin section only
		HookRegistrar::register('admin_enqueue_scripts', array($this, 'loadAssets'));
	}

	public function loadAssets()
	{
		Helper::load_assets('moxie-styles', '/css/admin.css');
	}
}