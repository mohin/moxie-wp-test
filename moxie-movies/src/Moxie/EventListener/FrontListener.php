<?php 

namespace Moxie\EventListener;

use Moxie\Entity\Movie;
use Moxie\Utils\FilterRegistrar;
use Moxie\Utils\HookRegistrar;
use Moxie\Utils\Helper;


class FrontListener {

	public function register()
	{
		// load assets only for front view
		if(! Helper::is_admin_section())
			$this->loadAssests();

		$movie = new Movie();
		// execute function on movieList call for both users and anonymos visitors 
		HookRegistrar::register('wp_ajax_movieList', array($movie, 'ajaxList'));
		HookRegistrar::register('wp_ajax_nopriv_movieList', array($movie, 'ajaxList'));

		// display posts if we are on the home page
		FilterRegistrar::register('the_content', array($movie, 'displayList'));
	}

	public function loadAssests()
	{
		Helper::load_assets('moxie-bootstrap', '/css/bootstrap.min.css');
		Helper::load_assets('moxie-angular-fx', '/css/angular-fx.css');
		Helper::load_assets('moxie-animate', '/css/animate.css');
		Helper::load_assets('moxie-front', '/css/front.css');
		Helper::load_assets('moxie-angular', '/js/angular.min.js', 'js');
		Helper::load_assets('moxie-app', '/js/app.js', 'js');
		Helper::load_assets('moxie-angular-fx', '/js/angular-fx.js', 'js');
	}
}