<!-- 
	Here we are passing operation to Angular with "moxie-movie-list" directive. 
	Look into assets/template/movie_list.html 
-->
<div class="container-fluid">
	<div class="row">
		<div ng-app="movieListApp" id="movie-list">
			<div ng-controller="movieListCtrl" data-ng-init="init()">
				
				<h2 class="movie-preload" ngfx-zoom-in-down="{{loaded}}" ng-hide="errors > 0">{{preload}}</h2>
				<h3 ngfx-zoom-in-up="errors > 0">{{error}}</h3>
				
				<moxie-movie-list ngfx-zoom-in-up="{{loaded}}"></moxie-movie-list>			
			</div>
		</div>
	</div>
</div>