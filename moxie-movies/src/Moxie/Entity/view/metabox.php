<?php 

	$movie_fields        = self::getFieldsName();
	$movie_fields_values = self::getFieldsData();

 ?>

<div class="moxie-movie-fields">

	<p>
		<label for="<?php echo $movie_fields['year'] ?>">Movie Year:</label>
		<input type="date" min="1920-01-01" max="<?php echo date('Y-m-d'); ?>" name="<?php echo $movie_fields['year']; ?>" value="<?php echo $movie_fields_values[$movie_fields['year']]; ?>">
	</p>
	
	<p>
		<label for="<?php echo $movie_fields['rating'] ?>">Movie Rating:</label>
		<input type="number" min="1" max="10" name="<?php echo $movie_fields['rating']; ?>" value="<?php echo $movie_fields_values[$movie_fields['rating']]; ?>">
	</p>

	<?php wp_nonce_field( 'saveMetabox', 'moxie-movie-nonce-field' ); ?>

</div>