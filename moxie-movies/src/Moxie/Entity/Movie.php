<?php 

namespace Moxie\Entity;

use Moxie\Utils\DBWrapper;
use Moxie\Utils\Helper;
use Moxie\Utils\Cache;


class Movie
{
	const POST_TYPE       = 'moxie-movie';
	
	// names for retriving cache
	const CACHE_LIST_NAME = 'movieList';
	
	const CACHE_LIST_META = 'movieListMeta';


	public function register()
	{
	  
		$args = $this->getPostArgs();
		
		if( !post_type_exists( static::POST_TYPE ) )
			register_post_type(static::POST_TYPE, $args);
	}


	public function getPostArgs()
	{
		$labels = array(
			'name'               => 'Movies',
			'singular_name'      => 'Movie',
			'add_new'            => 'Add Movie',
			'add_new_item'       => 'Add New Movie',
			'edit_item'          => 'Edit Movie',
			'new_item'           => 'New Movie',
			'view_item'          => 'View Movie',
			'search_items'       => 'Find Movie',
			'parent_item_colon'  => 'Parent Movie',
			'menu_name'          => 'Movies',
			'not_found'          => 'Movies not found',
			'not_found_in_trash' => 'Movies not found'
		  );
			
		$args = array(
			'labels'               => $labels,
			'public'               => true,
			'publicly_queryable'   => true,
			'show_ui'              => true,
			'show_in_menu'         => true,
			'menu_position'        => 5,
			'menu_icon'            => 'dashicons-video-alt',
			'query_var'            => true,
			'rewrite'              => array('slug' => 'movies','with_front' => false),
			'capability_type'      => 'post',
			'has_archive'          => true,
			'hierarchical'         => false,
			'supports'             => array('title', 'excerpt', 'thumbnail', 'movie-fields'),
			'register_meta_box_cb' => array($this, 'registerMetaBox')
	  );
		
		return $args;
	}


	public static function getFieldsName()
	{
		return array(
				'year'   => 'moxie-movie-year-field',
				'rating' => 'moxie-movie-rating-field',
				'nonse'  => 'moxie-movie-nonce-field'
			);
	}


	public static function getFieldsData()
	{
		$fields_data = array();
		$fields_name = self::getFieldsName();

		foreach ($fields_name as $field_name) {
			$field_data               = self::getMeta(self::currentID(), $field_name);
			$fields_data[$field_name] = $field_data;
		}

		return $fields_data;
	}


	public static function currentID()
	{
		return get_the_ID();
	}


	public function registerMetaBox()
	{
		add_meta_box(
				'movie-fields', 'Movie fields', array($this, 'renderMetabox'), static::POST_TYPE, 'advanced', 'high'
				);
	}


	public function renderMetabox()
	{
		require_once(__DIR__ . '/view/metabox.php');
	}


	public function requireTitle($title='')
	{
		if ( '' === $title )
			wp_die('Please set the a title field! Tap the "Back" button in your browser and try again.');

		return $title;
	}


	public function saveMetabox($post_id)
	{
	    $movie_fields = self::getFieldsName();

		// Check nonse, prevent autosave
		if (! isset( $_POST[$movie_fields['nonse']] ))
			return;

		if (! wp_verify_nonce( $_POST[$movie_fields['nonse']], 'saveMetabox' ))
			return;
		
		if (defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE) 
			return;

		// Prevent quick edit from clearing custom fields
	    if (defined('DOING_AJAX') && DOING_AJAX)
	        return;

	    // save meta data
	    if (isset( $_POST[$movie_fields['year']])) {

	    	$movie_year = sanitize_text_field($_POST[$movie_fields['year']] );

	    	//args: id, name in db, value
	    	self::updateMeta($post_id, $movie_fields['year'], $movie_year);
	    }

	    if (isset( $_POST[$movie_fields['rating']])) {

	    	$movie_rating = sanitize_text_field($_POST[$movie_fields['rating']] );

	    	self::updateMeta($post_id, $movie_fields['rating'], $movie_rating);
	    }
	}


	public static function updateMeta($id, $name, $val)
	{
		DBWrapper::update_meta($id, $name, $val);
	}


	public static function getMeta($id, $name)
	{
		return DBWrapper::get_meta($id, $name);
	}

	public function displayList()
	{
		// if home is not set
		if (!Helper::is_home_set() )
			wp_die("<h3>Please, asign the home page first.</h3>",'title');

		if( !Helper::is_home_page() )
			return;

		require_once(__DIR__ . '/view/movie_list.php');
	}

	public function getAll()
	{
		$args = array(
			'posts_per_page'   => -1,
			'post_type'   => static::POST_TYPE,
			'post_status' => 'publish'
			);

		return DBWrapper::get_entries($args);
	}

	public function getPoster($thumb_id)
	{
		return DBWrapper::get_thumb($thumb_id);
	}

	public function ajaxList()
	{
		$response     = array();
		$cache        = new Cache();
		$entry_fields = $this->getFieldsName();

		// check if data already cached
		if (is_null($cache->retrieve(static::CACHE_LIST_NAME))) {

			$entries      = $this->getAll();
			$entries_meta = array();

			foreach ($entries as $entry) {
				$entry_meta               = $this->getMeta($entry->ID, '');
				//check if poster added; if not, set a default image;
				if(isset($entry_meta['_thumbnail_id']))
					$entry_meta['poster_url'] = $this->getPoster($entry_meta['_thumbnail_id'][0]);
				else
					$entry_meta['poster_url'] = 'http://vaiden.net/moxieboy.gif';

				$entries_meta[$entry->ID] = $entry_meta;
			}
			
			$cache->store(static::CACHE_LIST_NAME, $entries);
			$cache->store(static::CACHE_LIST_META, $entries_meta);

		} else {
			// get cached data
			$entries      = $cache->retrieve(static::CACHE_LIST_NAME);
			$entries_meta = $cache->retrieve(static::CACHE_LIST_META);
		}

		// if there are no entries
		if (empty($entries)) {
			$response['error'] = 'Sorry, no movies yet.';
		} else {

			foreach ($entries as $entry) {
				$entry_meta = $entries_meta[$entry->ID];
				$response[] = array(
					'id'                => $entry->ID,
					'title'             => $entry->post_title,
					'poster_url'        => $entry_meta['poster_url'],
					'rating'            => $entry_meta[$entry_fields['rating']][0],
					'year'              =>	$entry_meta[$entry_fields['year']][0],
					'short_description' => $entry->post_excerpt
					);
			}
		}

		// send JSON and die()
		wp_send_json($response);

	}

}